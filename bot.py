import keyinput, time, win32gui, win32api, win32con, win32com.client, win32process, cv2, sys, ctypes
from PIL import ImageGrab
import numpy as np
import os, signal, threading

# The template inside the name of the window we are looking for
# Yes, this clusterfuck is the standard name 'Call of Duty®: Modern Warfare®',
# With lots of Zero Width Spaces (U+200B) inserted inbetween, big Infinity-Ward anti-cheat !
window_template = '\u0043\u200b\u0061\u200b\u006c\u200b\u006c\u200b\u0020\u200b\u006f\u200b\u0066\u200b\u0020\u200b' \
                  '\u0044\u200b\u0075\u200b\u0074\u200b\u0079\u200b\u00ae\u200b\u003a\u200b\u0020\u200b\u004d\u200b' \
                  '\u006f\u200b\u0064\u200b\u0065\u200b\u0072\u200b\u006e\u200b\u0020\u200b\u0057\u200b\u0061\u200b' \
                  '\u0072\u200b\u0066\u200b\u0061\u200b\u0072\u200b\u0065\u200b\u00ae'

# The name of the "Start in safe mode" window,
# in english (feel free to modify into your mother tongue)
safe_mode_window_name = 'Run In Safe Mode?'

# The name of the "DEV ERROR" windows (usually related to DirectX errors),
# in english (feel free to modify into your mother tongue)
dev_error_window_name = 'Fatal Error'

# The name of the battle.net window
battle_net_window_name = 'Blizzard Battle.net'

# The complete name of the window that contains this template
window_name = None

# Position of the modern warfare window
window_x = -1
window_y = -1
window_width = -1
window_height = -1

# Windows process related stuff
window_pid = -1
window_path = ""
window_found = False

# Routine to kill a process, used when it gets frozen permanently
def killprocess(pid):
    os.kill(pid, signal.SIGTERM)


# Callback for windows enumeration
def getmwwindow(hwnd, extra):
    w_name = win32gui.GetWindowText(hwnd)

    if window_template not in w_name:
        return

    global window_name
    window_name = w_name

    global window_found
    window_found = True

    global window_pid
    global window_path
    thread_id, window_pid = win32process.GetWindowThreadProcessId(hwnd)

    handle = win32api.OpenProcess(win32con.PROCESS_QUERY_INFORMATION | win32con.PROCESS_VM_READ, False, window_pid)
    window_path = win32process.GetModuleFileNameEx(handle, 0)

    rect = win32gui.GetClientRect(hwnd)
    window_pos = win32gui.ClientToScreen(hwnd, (0, 0))
    global window_x
    global window_y
    global window_width
    global window_height
    window_x = window_pos[0]
    window_y = window_pos[1]
    window_width = rect[2]
    window_height = rect[3]

    # Check for whether or not the window has the right resolution
    if window_width != 1280 or window_height != 720:
        # If not, resize the window.
        win32gui.SetWindowPos(hwnd, 0, window_x, window_y, 1296, 759, 0)
        window_width = 1280
        window_width = 720


# Keys to press to move around
keys = [ keyinput.W, keyinput.D, keyinput.S, keyinput.A ]


# Function to move around ingame
def move_around():
    for key in keys:
        keyinput.holdKey(key, 1.0)

# Function to press space
def press_space_delayed(x, y):
    # Wait for 3 seconds
    time.sleep(3.0)
    keyinput.pressKey(keyinput.SPACE)
    time.sleep(0.05)
    keyinput.releaseKey(keyinput.SPACE)

# Function to delay further the click after the cursor has been moved,
# in case we can't rely on a hovered version of a button
def click_delayed_3_seconds(x, y):
    keyinput.click(x, y, 3.0)

# Find a certain item in an image, returns the estimated position and associated threshold
def findItem(img, template):
    # Apply template Matching
    res = cv2.matchTemplate(img, template, cv2.TM_CCOEFF_NORMED)

    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    w, h = template.shape[::-1]

    # Top left of the area
    top_left = max_loc
    # Buttom right of the area
    bottom_right = (top_left[0] + w, top_left[1] + h)

    # Zone to click if we do need
    middle_x = (top_left[0] + bottom_right[0]) / 2
    middle_y = (top_left[1] + bottom_right[1]) / 2

    return {'threshold' : max_val, 'x' : middle_x, 'y' : middle_y}


# Take a screenshot of the window
def screenshot(x, y, width, height):
    image = ImageGrab.grab(bbox = (x, y, x + width, y + height))
    image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
    return image

# The previous image used in the watchdog
old_img = None

# The "No" button of the "Start in safe mode" window
safe_mode_no_button = cv2.imread("Images/Extras/safe_mode_no_button.png", cv2.IMREAD_GRAYSCALE)

# The "OK" button of dev errors
dev_error_ok_button = cv2.imread("Images/Extras/dev_error_ok_button.png", cv2.IMREAD_GRAYSCALE)

# The play button in the battle net window
battle_net_play_button = cv2.imread("Images/Extras/battle_net_play_button.png", cv2.IMREAD_GRAYSCALE)
battle_net_play_button_hovered = cv2.imread("Images/Extras/battle_net_play_button_hovered.png", cv2.IMREAD_GRAYSCALE)

# Check for a safe mode window and proceed accordingly to close it if present, returns true if it was
def checkForSafeModeWindow() -> bool:
    # Try to find the window handle
    hwnd = win32gui.FindWindow(None, safe_mode_window_name)

    if hwnd == 0:
        return False

    rect = win32gui.GetClientRect(hwnd)
    window_pos = win32gui.ClientToScreen(hwnd, (0, 0))

    # Put it in foreground in case it is not and wait for a bit to ensure it is made visible
    # Workaround for foreground windows
    shell = win32com.client.Dispatch("WScript.Shell")
    shell.SendKeys('%')
    win32gui.SetForegroundWindow(hwnd)
    time.sleep(1.0)

    # Screenshot the safe mode window
    safe_mode_window_img = screenshot(window_pos[0], window_pos[1], rect[2], rect[3])

    # Convert it to grayscale for faster processing
    img = cv2.cvtColor(safe_mode_window_img, cv2.COLOR_BGR2GRAY)

    # Try to find the "No" button
    result = findItem(img, safe_mode_no_button)

    # Click on it if was found
    if result['threshold'] > 0.90:
        keyinput.click(window_pos[0] + int(result['x']), window_pos[1] + int(result['y']))
        return True

    return False

# Check for a DEV Error window
def checkForDevError() -> bool:
    # Try to find the window handle
    hwnd = win32gui.FindWindow(None, dev_error_window_name)

    if hwnd == 0:
        return False

    rect = win32gui.GetClientRect(hwnd)
    window_pos = win32gui.ClientToScreen(hwnd, (0, 0))

    # Put it in foreground in case it is not and wait for a bit to ensure it is made visible
    # Workaround for foreground windows
    shell = win32com.client.Dispatch("WScript.Shell")
    shell.SendKeys('%')
    win32gui.SetForegroundWindow(hwnd)
    time.sleep(1.0)

    # Screenshot the safe mode window
    dev_error_window_img = screenshot(window_pos[0], window_pos[1], rect[2], rect[3])

    # Convert it to grayscale for faster processing
    img = cv2.cvtColor(dev_error_window_img, cv2.COLOR_BGR2GRAY)

    # Try to find the "OK" button
    result = findItem(img, dev_error_ok_button)

    # Click on it if was found
    if result['threshold'] > 0.90:
        keyinput.click(window_pos[0] + int(result['x']), window_pos[1] + int(result['y']))
        return True

    return False

# Check for the play button in the battle net window
def checkForBattleNetPlayButton() -> bool:
    # Try to find the window handle
    hwnd = win32gui.FindWindow(None, battle_net_window_name)

    if hwnd == 0:
        return False

    rect = win32gui.GetClientRect(hwnd)
    window_pos = win32gui.ClientToScreen(hwnd, (0, 0))

    # Put it in foreground in case it is not and wait for a bit to ensure it is made visible
    # Workaround for foreground windows
    shell = win32com.client.Dispatch("WScript.Shell")
    shell.SendKeys('%')
    win32gui.SetForegroundWindow(hwnd)
    time.sleep(1.0)

    # Screenshot the safe mode window
    battle_net_window_img = screenshot(window_pos[0], window_pos[1], rect[2], rect[3])

    # Convert it to grayscale for faster processing
    img = cv2.cvtColor(battle_net_window_img, cv2.COLOR_BGR2GRAY)

    # Try to find the "Play" button
    result = findItem(img, battle_net_play_button)

    # Click on it if was found
    if result['threshold'] > 0.90:
        keyinput.click(window_pos[0] + int(result['x']), window_pos[1] + int(result['y']))
        return True

    # Try to find the "Play" button if it is hovered
    result = findItem(img, battle_net_play_button_hovered)

    # Click on it if was found
    if result['threshold'] > 0.90:
        keyinput.click(window_pos[0] + int(result['x']), window_pos[1] + int(result['y']))
        return True

    return False

# Watchdog running on another thread, makes sure the Apex process didn't crash or is not stucked
def processwatchdog():
    while True:
        # Check for the process being alive every 60 seconds
        time.sleep(60)

        global window_pid
        global window_path
        global window_found

        hwnd = 0

        # Try to find the window handle
        if window_name is not None:
            hwnd = win32gui.FindWindow(None, window_name)

        # Make sure the Window is still alive
        if hwnd == 0:
            window_found = False

            # Check for the "start in safe mode" window in case we are already launching the game
            if checkForSafeModeWindow() is True:
                continue

            # Check for a DEV Error in case it happened
            if checkForDevError() is True:
                continue

            # Check for the battle net window
            if checkForBattleNetPlayButton() is True:
                continue

            # In case the window name changed slightly (P100 Infinity Ward anti cheat technique), try to re-fetch it
            win32gui.EnumWindows(getmwwindow, None)

            # If it is still not found, wait again
            if window_found is False:
                continue

        # Refresh the window's position
        win32gui.EnumWindows(getmwwindow, None)

        # Grab its handle
        hwnd = win32gui.FindWindow(None, window_name)

        # Put it in foreground
        # Workaround for foreground windows
        shell = win32com.client.Dispatch("WScript.Shell")
        shell.SendKeys('%')
        win32gui.SetForegroundWindow(hwnd)

        global old_img

        # Capture the window in color
        color_img = screenshot(window_x, window_y, window_width, window_height)

        # Convert it to grayscale for faster processing
        img = cv2.cvtColor(color_img, cv2.COLOR_BGR2GRAY)

        if (old_img is not None):
            # Compare the current image to the old one,
            # if it didn't change within 60 seconds, the process is likely stuck
            result = findItem(img, old_img)

            # We deem the image as being the same if it matches over 99.999%
            if result['threshold'] > 0.99999:
                # Kill the process if it matches
                killprocess(window_pid)

        # Save the current image to compare it to the next one being read
        old_img = img


# Templates that we are looking for in the image
warzone_left_button = cv2.imread("Images/warzone_left_button.png", cv2.IMREAD_GRAYSCALE)
warzone_right_button = cv2.imread("Images/warzone_right_button.png", cv2.IMREAD_GRAYSCALE)
warzone_play_button = cv2.imread("Images/warzone_play_button.png", cv2.IMREAD_GRAYSCALE)
play_button = cv2.imread("Images/play_button.png", cv2.IMREAD_GRAYSCALE)
space_prompt = cv2.imread("Images/space_prompt.png", cv2.IMREAD_GRAYSCALE)
play_again_button = cv2.imread("Images/play_again_button.png", cv2.IMREAD_GRAYSCALE)
play_again_button_hovered = cv2.imread("Images/play_again_button_hovered.png", cv2.IMREAD_GRAYSCALE)
leave_game_confirm_yes_button = cv2.imread("Images/leave_game_confirm_yes_button.png", cv2.IMREAD_GRAYSCALE)
leave_game_confirm_yes_button_hovered = cv2.imread("Images/leave_game_confirm_yes_button_hovered.png", cv2.IMREAD_GRAYSCALE)
leave_game_with_party_button = cv2.imread("Images/leave_game_with_party_button.png", cv2.IMREAD_GRAYSCALE)
leave_game_with_party_button_hovered = cv2.imread("Images/leave_game_with_party_button_hovered.png", cv2.IMREAD_GRAYSCALE)
leave_with_party_button = cv2.imread("Images/leave_with_party_button.png", cv2.IMREAD_GRAYSCALE)
leave_with_party_button_hovered = cv2.imread("Images/leave_with_party_button_hovered.png", cv2.IMREAD_GRAYSCALE)
continue_button = cv2.imread("Images/continue_button.png", cv2.IMREAD_GRAYSCALE)
continue_button_hovered = cv2.imread("Images/continue_button_hovered.png", cv2.IMREAD_GRAYSCALE)
pause_shader_installation_button = cv2.imread("Images/pause_shader_installation_button.png", cv2.IMREAD_GRAYSCALE)
pause_shader_installation_button_hovered = cv2.imread("Images/pause_shader_installation_button_hovered.png", cv2.IMREAD_GRAYSCALE)
kicked_for_inactivity_button = cv2.imread("Images/kicked_for_inactivity_button.png", cv2.IMREAD_GRAYSCALE)
kicked_for_inactivity_button_hovered = cv2.imread("Images/kicked_for_inactivity_button_hovered.png", cv2.IMREAD_GRAYSCALE)
announcement_close_button = cv2.imread("Images/announcement_close_button.png", cv2.IMREAD_GRAYSCALE)
announcement_close_button_hovered = cv2.imread("Images/announcement_close_button_hovered.png", cv2.IMREAD_GRAYSCALE)

# Additional templates that may be added depending on what the user selects

# When queuing for modes where you have to select a loadout (e.g. Plunder)
my_loadout_button = cv2.imread("Images/my_loadout_button.png", cv2.IMREAD_GRAYSCALE)
my_loadout_button_hovered = cv2.imread("Images/my_loadout_button_hovered.png", cv2.IMREAD_GRAYSCALE)

# When queuing one of the "old school 150 players" battle royale modes
battle_royale_button = cv2.imread("Images/battle_royale_button.png", cv2.IMREAD_GRAYSCALE)

# When queuing the battle royale quads with 200 players mode
br_quads_button = cv2.imread("Images/br_quads_button.png", cv2.IMREAD_GRAYSCALE)
br_quads_button_hovered = cv2.imread("Images/br_quads_button_hovered.png", cv2.IMREAD_GRAYSCALE)
br_quads_ui_elements = [{"image" : br_quads_button, "threshold" : 0.80, "callback" : keyinput.click},
                        {"image" : br_quads_button_hovered, "threshold" : 0.85, "callback" : keyinput.click},
                        {"image" : battle_royale_button, "threshold" : 0.80, "callback" : keyinput.move}]

# When queuing for the battle royale trios mode
br_trios_button = cv2.imread("Images/br_trios_button.png", cv2.IMREAD_GRAYSCALE)
br_trios_button_hovered = cv2.imread("Images/br_trios_button_hovered.png", cv2.IMREAD_GRAYSCALE)
br_trios_ui_elements = [{"image" : br_trios_button, "threshold" : 0.80, "callback" : keyinput.click},
                        {"image" : br_trios_button_hovered, "threshold" : 0.85, "callback" : keyinput.click},
                        {"image" : battle_royale_button, "threshold" : 0.80, "callback" : keyinput.move}]

# When queuing for the battle royale duos mode
br_duos_button = cv2.imread("Images/br_duos_button.png", cv2.IMREAD_GRAYSCALE)
br_duos_button_hovered = cv2.imread("Images/br_duos_button_hovered.png", cv2.IMREAD_GRAYSCALE)
br_duos_ui_elements = [{"image" : br_duos_button, "threshold" : 0.80, "callback" : keyinput.click},
                       {"image" : br_duos_button_hovered, "threshold" : 0.85, "callback" : keyinput.click},
                       {"image" : battle_royale_button, "threshold" : 0.80, "callback" : keyinput.move}]

# When queuing for the battle royale buy back solos mode
br_solos_button = cv2.imread("Images/br_solos_button.png", cv2.IMREAD_GRAYSCALE)
br_solos_button_hovered = cv2.imread("Images/br_solos_button_hovered.png", cv2.IMREAD_GRAYSCALE)
br_solos_ui_elements = [{"image" : br_solos_button, "threshold" : 0.80, "callback" : keyinput.click},
                        {"image" : br_solos_button_hovered, "threshold" : 0.85, "callback" : keyinput.click},
                        {"image" : battle_royale_button, "threshold" : 0.80, "callback" : keyinput.move}]

# When queuing for the plunder quads mode
plunder_blood_money_button = cv2.imread("Images/plunder_blood_money_button.png", cv2.IMREAD_GRAYSCALE)
plunder_blood_money_button_hovered = cv2.imread("Images/plunder_blood_money_button_hovered.png", cv2.IMREAD_GRAYSCALE)
plunder_blood_money_ui_elements = [{"image" : plunder_blood_money_button, "threshold" : 0.80, "callback" : keyinput.click},
                                   {"image" : plunder_blood_money_button_hovered, "threshold" : 0.85, "callback" : keyinput.click},
                                   {"image" : my_loadout_button, "threshold" : 0.80, "callback" : keyinput.click},
                                   {"image" : my_loadout_button_hovered, "threshold" : 0.85, "callback" : keyinput.click}]

# When queuing for the rebirth mini royale quads mode
rebirth_mini_royale_quads_button = cv2.imread("Images/rebirth_mini_royale_quads_button.png", cv2.IMREAD_GRAYSCALE)
rebirth_mini_royale_quads_button_hovered = cv2.imread("Images/rebirth_mini_royale_quads_button_hovered.png", cv2.IMREAD_GRAYSCALE)
rebirth_mini_royale_quads_ui_elements = [{"image" : rebirth_mini_royale_quads_button, "threshold" : 0.80, "callback" : keyinput.click},
                                         {"image" : rebirth_mini_royale_quads_button_hovered, "threshold" : 0.85, "callback" : keyinput.click}]

optional_ui_elements = {"br-quads" : br_quads_ui_elements,
                        "br-trios" : br_trios_ui_elements,
                        "br-duos" : br_duos_ui_elements,
                        "br-solos" : br_solos_ui_elements,
                        "rebirth-mini-royale-quads" : rebirth_mini_royale_quads_ui_elements,
                        "plunder-blood-money" : plunder_blood_money_ui_elements}

UI_elements = [warzone_left_button,
               warzone_right_button,
               warzone_play_button,
               play_button,
               space_prompt,
               play_again_button,
               play_again_button_hovered,
               leave_game_confirm_yes_button,
               leave_game_confirm_yes_button_hovered,
               leave_game_with_party_button,
               leave_game_with_party_button_hovered,
               leave_with_party_button,
               leave_with_party_button_hovered,
               continue_button,
               continue_button_hovered,
               pause_shader_installation_button,
               pause_shader_installation_button_hovered,
               kicked_for_inactivity_button,
               kicked_for_inactivity_button_hovered,
               announcement_close_button,
               announcement_close_button_hovered]

# Associated thresholds, > 1.0 is unreachable
UI_thresholds = [0.95, # warzone_left_button
                 0.95, # warzone_right_button
                 0.95, # warzone_play_button
                 0.95, # play_button
                 0.90, # space_prompt
                 0.75, # play_again_button
                 0.95, # play_again_button_hovered
                 0.75, # leave_game_confirm_yes_button
                 0.95, # leave_game_confirm_yes_button_hovered
                 0.75, # leave_game_with_party_button
                 0.95, # leave_game_with_party_button_hovered
                 0.75, # leave_with_party_button
                 0.95, # leave_with_party_button_hovered
                 0.75, # continue_button
                 0.95, # continue_button_hovered
                 0.75, # pause_shader_installation_button
                 0.95, # pause_shader_installation_button_hovered
                 0.75, # kicked_for_inactivity_button
                 0.95, # kicked_for_inactivity_button_hovered
                 0.95, # announcement_close_button
                 0.95] # announcement_close_button_hovered

# Associated callbacks
UI_callbacks = [keyinput.click, # warzone_left_button
                keyinput.click, # warzone_right_button
                keyinput.click, # warzone_play_button
                click_delayed_3_seconds, # play_button
                press_space_delayed, # space_prompt
                keyinput.click, # play_again_button
                keyinput.click, # play_again_button_hovered
                keyinput.click, # leave_game_confirm_yes_button
                keyinput.click, # leave_game_confirm_yes_button_hovered
                keyinput.click, # leave_game_with_party_button
                keyinput.click, # leave_game_with_party_button_hovered
                keyinput.click, # leave_with_party_button
                keyinput.click, # leave_with_party_button_hovered
                keyinput.click, # continue_button
                keyinput.click, # continue_button_hovered
                keyinput.click, # pause_shader_installation_button
                keyinput.click, # pause_shader_installation_button_hovered
                keyinput.click, # kicked_for_inactivity_button
                keyinput.click, # kicked_for_inactivity_button_hovered
                keyinput.click, # announcement_close_button
                keyinput.click] # announcement_close_button_hovered

# In case the user selected a gamemode, pick the right one
if len(sys.argv) > 1:
    if optional_ui_elements.__contains__(sys.argv[1]):
        print("Selected gamemode: " + sys.argv[1])
        for ui_element in optional_ui_elements[sys.argv[1]]:
            UI_elements.append(ui_element["image"])
            UI_thresholds.append(ui_element["threshold"])
            UI_callbacks.append(ui_element["callback"])
    else:
        print("Unknown gamemode: '" + sys.argv[1] + "', exiting.")
        sys.exit(1)
else:
    print("No gamemode specified, defaulting to 'BR Quads'")
    for ui_element in optional_ui_elements["br-quads"]:
        UI_elements.append(ui_element["image"])
        UI_thresholds.append(ui_element["threshold"])
        UI_callbacks.append(ui_element["callback"])

# Set ourselves as DPI aware, or else we won't get proper pixel coordinates if scaling is not 100%
errorCode = ctypes.windll.shcore.SetProcessDpiAwareness(2)

print("Modern Warfare bot starting in 5 seconds, bring Modern Warfare window in focus...")
time.sleep(5)

win32gui.EnumWindows(getmwwindow, None)

# If the window hasn't been found, exit
if window_found == False:
    print("No Modern Warfare window found")
    sys.exit(1)

# Start the watchdog thread
watchdogthread = threading.Thread(target = processwatchdog)
watchdogthread.start()

while True:
    # If the window died, stop running analysis for a moment */
    if window_found is False:
        time.sleep(5)
        continue

    # Capture the window in color
    color_img = screenshot(window_x, window_y, window_width, window_height)
    # Convert it to grayscale for faster processing
    img = cv2.cvtColor(color_img, cv2.COLOR_BGR2GRAY)

    found_UI_element = False

    # Try to find any of the templates
    for template, threshold, callback in zip(UI_elements, UI_thresholds, UI_callbacks):
        result = findItem(img, template)

        if result['threshold'] > threshold:
            found_UI_element = True
            callback(window_x + int(result['x']), window_y + int(result['y']))
            break

    # If no UI element got found, move around is the default behavior
    if found_UI_element is False:
        move_around()

    # Sleep for a second
    time.sleep(1.0)